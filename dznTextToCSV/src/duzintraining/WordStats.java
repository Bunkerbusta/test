package duzintraining;

public class WordStats {
	/*
	(a) Класс WordStats содержит поля word (само слово), count (кол-во повторений слова в тексте)
	*/
	private String word;
	private int count;

	public WordStats(String word) {
		this.word = word;
		count = 1;
	}

	public WordStats(String word, int count) {
		this.word = word;
		this.count = count;
	}

	public String getWord() {
		return word;
	}

	public int getCount() {
		return count;
	}

	public double getFrequency(int totalWordCount) {
		return (100d * (double) count / totalWordCount);
	}

	public void add() {
		count++;
	}
}