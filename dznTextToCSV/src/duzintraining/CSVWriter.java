package duzintraining;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

public class CSVWriter {
	/*
	(3) Получает SortedSet, общее кол-во слов и пишет в указанный CSV файл в формате $Word,$Count,$Percentage,
	используя заданный сепаратор
	 */

	static final char CSV_SEPARATOR = ',';
	private DecimalFormat formatter;

	public CSVWriter() {
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
		otherSymbols.setDecimalSeparator('.');
		formatter = new DecimalFormat("#.###", otherSymbols);
		formatter.setRoundingMode(RoundingMode.CEILING);
	}

	private String formatCSV(Set<WordStats> words, int totalWordCount, char csvSeparator) {
		Iterator<WordStats> iterator = words.iterator();
		StringBuilder csvLines = new StringBuilder();
		while(iterator.hasNext()) {
			WordStats wc = iterator.next();

			// append CSV formatted line
			csvLines.append(wc.getWord());
			csvLines.append(csvSeparator);
			csvLines.append(wc.getCount());
			csvLines.append(csvSeparator);
			csvLines.append(formatter.format(wc.getFrequency(totalWordCount)));

			// append new line
			csvLines.append(System.getProperty("line.separator"));
		}

		return csvLines.toString();
	}

	public void print(Set<WordStats> words, int totalWordsCount) {
		System.out.println( formatCSV(words, totalWordsCount, CSV_SEPARATOR) );
	}

	public void writeToFile(Set<WordStats> words, int totalWordsCount, String filename, char csvSeparator)
			throws Exception {
		if (filename.isEmpty()) {
			throw new IllegalArgumentException("Output file name is empty!");
		}

		if (words.size() == 0) {
			throw new IllegalArgumentException("Words statistics is empty! Nothing to write to output file.");
		}

		if (totalWordsCount == 0) {
			throw new Exception("Total words count should be more than 0!");
		}

		// Try-with-resources block -- will close reader on try block finished/exception occur
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
			writer.write( formatCSV(words, totalWordsCount, csvSeparator) );
		} catch (Exception e) {
			throw new IOException(String.format("Failed to write to output file \"%s\"!", filename));
		}
	}
}
