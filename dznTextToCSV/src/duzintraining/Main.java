package duzintraining;

/*
	1. TextfileReader
	2. WordsStatisticsProcessor
	3. CSVWriter
	4. FileProcessor
	a. WordStats

	(1) Считывает файл по-символьно, преобразуя набор символов в слова каждый раз, когда символ не буква-цифра.
	На выходе получаем ArrayList всех встреченных слов.

	(2) Получает ArrayList всех слов, преобразует его в Map формата <Слово, Элемент подсчета> (a)
	(проходит по всему ArrayList, преобразует в ловеркейс и обновляет/добавляет элемет в Map).
	После подсчета слов - преобразует в сортированный список (по убыванию кол-ва повторений и алфавиту).
	На выходе получаем SortedSet<WordStats>.

	(3) Получает SortedSet, общее кол-во слов и пишет в указанный CSV файл в формате $Word,$Count,$Percentage,
	используя заданный сепаратор

	(4) Получает имена входного и выходного файлов, а также сепаратор.
	Вычитывает текст из файла и пишет статистику в выходной csv файл.

	(a) Класс WordStats содержит поля word (само слово), count (кол-во повторений слова в тексте)
 */

public class Main {
	public static void main(String[] args) {
		FileProcessor fileProcessor = new FileProcessor();
		fileProcessor.exportWordsStatistics("testFile.txt", "result.csv", ',');
	}
}