package duzintraining;

import java.util.*;

public class WordsStatisticsProcessor {
	/*
	(2) Получает ArrayList всех слов, преобразует его в Map формата <Слово, Элемент подсчета> (a)
	(проходит по всему ArrayList, преобразует в ловеркейс и обновляет/добавляет элемет в Map).
	После подсчета слов - преобразует в сортированный список (по убыванию кол-ва повторений и алфавиту).
	На выходе получаем SortedSet<WordStats>.
	 */

	private SortedSet<WordStats> sortedWords;

	public void processWordList(ArrayList<String> list) {
		if (list.size() == 0) {
			throw new IllegalArgumentException("Word list is empty!");
		}

		// Generate map of WordStats items and counting word repetition
		Map<String, WordStats> words = convertToWordsMap(list);

		// Sort & update word frequency values
		sortedWords = processToSortedSet(words);
	}

	public SortedSet<WordStats> getSortedWords() {
		return sortedWords;
	}

	private Map<String, WordStats> convertToWordsMap(ArrayList<String> list) {
		Map<String, WordStats> words = new HashMap<>();

		for (int i = 0; i < list.size(); i++) {
			String word = list.get(i).toLowerCase();

			if (words.containsKey(word)) {
				// Update map: +1 to related WordStats
				WordStats wc = words.get(word);
				wc.add();
			} else {
				// Add new item
				words.put(word, new WordStats(word));
			}
		}

		return words;
	}

	private SortedSet<WordStats> processToSortedSet(Map<String, WordStats> words) {
		if (words.size() == 0) {
			throw new IllegalArgumentException("Failed to sort words by frequency due to empty word statistics list!");
		}

		SortedSet<WordStats> sortedSet = new TreeSet<>( getDescendingComparator() );
		for (Map.Entry wcEntry : words.entrySet()) {
			WordStats wc = (WordStats) wcEntry.getValue();
			sortedSet.add(wc);
		}

		return sortedSet;
	}

	private Comparator<WordStats> getDescendingComparator() {
		Comparator<WordStats> descendingComparator = new Comparator<WordStats>() {
			@Override
			public int compare(WordStats o1, WordStats o2) {
				int countCompare = (o2.getCount() - o1.getCount());
				// --- Sorts by count or alphabetically if counts are the same
				if (countCompare != 0) {
					return countCompare;
				} else {
					return (o1.getWord()).compareTo(o2.getWord());
				}
			}
		};

		return descendingComparator;
	}
}
