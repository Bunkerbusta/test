package duzintraining;

import java.io.*;
import java.util.ArrayList;

public class TextfileReader {
	/*
	(1) Считывает файл по-символьно, преобразуя набор символов в слова каждый раз, когда символ не буква-цифра.
	На выходе получаем ArrayList всех встреченных слов.
	 */

	private void convertToWordAndAddToList(StringBuilder word, ArrayList<String> words) {
		if (word.length() > 0) {
			words.add(word.toString());
			word.delete(0, word.length());
		}
	}

	public ArrayList<String> getWordsFromTextFile(String filename) throws IOException {
		File file = new File(filename);

		if (!file.exists()) {
			throw new FileNotFoundException(String.format("Input file \"%s\" not found", filename));
		}
		if (file.length() == 0) {
			throw new IOException(String.format("Input file \"%s\" is empty", filename));
		}

		ArrayList<String> list = new ArrayList<>();;

		// Try-with-resources block -- will close reader on try block finished/exception occur
		try (Reader reader = new InputStreamReader(new FileInputStream(file))) {

			StringBuilder word = new StringBuilder();
			int i = reader.read();
			while ( i != -1) {
				if (Character.isLetterOrDigit((char) i)) {
					// Char is part of word
					word.append((char) i);
				} else {
					// Char is delimeter, convert & clear not empty StringBuilder to string
					convertToWordAndAddToList(word, list);
				}

				i = reader.read();
			}

			// Converting last symbols to word on file end
			convertToWordAndAddToList(word, list);

		} catch (Exception e) {
			throw new IOException(String.format("Failed to read input file \"%s\"!", filename));
		}

		return list;
	}
}
