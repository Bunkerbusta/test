package duzintraining;

import java.util.ArrayList;
import java.util.Set;

public class FileProcessor {
	/*
	(4) Получает имена входного и выходного файлов, а также сепаратор.
	Вычитывает текст из файла и пишет статистику в выходной csv файл.
	 */

	private TextfileReader reader;
	private CSVWriter writer;
	private WordsStatisticsProcessor wordsStatsProcessor;

	public FileProcessor() {
		reader = new TextfileReader();
		writer = new CSVWriter();
		wordsStatsProcessor = new WordsStatisticsProcessor();
	}

	public void exportWordsStatistics(String inputFile, String outputFile, char csvSeparator) {
		try {
			if (inputFile.isEmpty() || outputFile.isEmpty()) {
				throw new IllegalArgumentException((inputFile.isEmpty()) ? "Empty input file name!" : "Empty output file name!");
			}

			ArrayList<String> words = reader.getWordsFromTextFile(inputFile);

			wordsStatsProcessor.processWordList(words);
			Set<WordStats> sortedWordStats = wordsStatsProcessor.getSortedWords();

			writer.writeToFile(sortedWordStats, words.size(), outputFile, csvSeparator);

			System.out.println("Word statistics:");
			System.out.println("Total: " + (words.size()));
			System.out.println("Unique: " + (sortedWordStats.size()));
		} catch (Exception e) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage());
			return;
		}

		System.out.println("Word statistics successfully exported!");
	}
}
